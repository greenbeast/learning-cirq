#Using this tutorial https://cirq.readthedocs.io/en/stable/tutorial.html
import cirq as c

#Using a named Qubit because it is useful for abstract algorithms
#and this algo hasn't been mapped to hardware.
q0 = c.NamedQubit('source')
q1 = c.NamedQubit('target')

#Line qubit

q3 = c.LineQubit(3)

#Can also make them in a range, this creates
#LineQubits 1-3
q0,q1,q2 = c.LineQubit.range(3)

#Grid qubit
q4_5 = c.GridQubit(4,5)

#make in bulk in a square
#makes 16 qubits from 0,0 to 3,3
qubits = c.GridQubit.square(4)

#print(c.google.Foxtail)

#Example Gates
not_gate = c.CNOT
pauli_z = c.Z

#Using exponentials to get square roots
sqrt_x_gate = c.X**0.5
sqrt_iswap = c.ISWAP**0.5

#Some gates can also take parameters
sqrt_sqrt_y = c.YPowGate(exponent=0.25)

#example operations
q0,q1 = c.LineQubit.range(2)

z_op = c.Z(q0)
not_op = c.CNOT(q0,q1)
sqrt_iswap_op = sqrt_iswap(q0,q1)
#print(z_op, '\n', not_op, '\n', sqrt_iswap_op)

#This makes a circuit then appends to it.
circuit = c.Circuit()
circuit.append(c.H(q) for q in c.LineQubit.range(3))
#print(circuit,"\n")

#WE can also make a circuit directly.
#print(c.Circuit(c.SWAP(q,q+1) for q in c.LineQubit.range(3)))

#Creates a circuit to simulate a bell state
bell_circuit = c.Circuit()
q0,q1 = c.LineQubit.range(2)
bell_circuit.append(c.H(q0))
bell_circuit.append(c.CNOT(q0,q1))

s = c.Simulator()
results = s.simulate(bell_circuit)
#print(f"Simulation of the circuit: \n {results}")

#For sampling we need to add a measurement at the end
bell_circuit.append(c.measure(q0,q1,key='result'))
samples = s.run(bell_circuit, repetitions=1000)
print(f"Sample of the circuit: \n {samples.histogram(key='result')}")


